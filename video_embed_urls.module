<?php

/**
 * @file
 * Adds a handler for direct URLs. Useful for Amazon Web Services (AWS) S3 videos to Video Embed Field.
 */

/**
 * Implements hook_video_embed_handler_info().
 */
function video_embed_urls_video_embed_handler_info() {
  $handlers = array();
  $handlers['allowed_urls'] = array(
    'title' => 'Allowed URLs',
    'function' => 'video_embed_urls_handle_video',
    'thumbnail_function' => 'video_embed_urls_handle_thumbnail',
    'thumbnail_default' => drupal_get_path('module', 'video_embed_urls') . '/img/amazon.jpg',
    'form' => 'video_embed_urls_form',
    'form_validate' => 'video_embed_field_handler_urls_form_validate',
    'domains' => array(
      variable_get('video_embed_urls', '').'.amazonaws.com',
      'test.media.s3.amazonaws.com',
    ),
    'defaults' => array(
      'width' => 640,
      'height' => 360,
      'class' => '',
    ),
  );

  return $handlers;
}

/**
 * Defines the form elements for the Allowed URLs videos configuration form.
 *
 * @param array $defaults
 *   The form default values.
 *
 * @return array
 *   The provider settings form array.
 */
function video_embed_urls_form($defaults) {
  $form = array();

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Width'),
    '#description' => t('The width of the player.'),
    '#default_value' => $defaults['width'],
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Height'),
    '#description' => t('The height of the player.'),
    '#default_value' => $defaults['height'],
  );

  $form['class'] = array(
    '#type' => 'textfield',
    '#title' => t('Player CSS class'),
    '#description' => t('CSS class to add to the player'),
    '#default_value' => $defaults['class'],
  );

  return $form;
}

/**
 * Validates the form elements for the Allowed URLs video configuration form.
 *
 * @param array $element
 *   The form element to validate.
 * @param array $form_state
 *   The form to validate state.
 * @param array $form
 *   The form to validate structure.
 */
function video_embed_field_handler_urls_form_validate($element, &$form_state, $form) {
  video_embed_field_validate_dimensions($element);
}

/**
 * Handler for Allowed URLs videos.
 *
 * @param string $url
 *   The video URL.
 * @param array $settings
 *   The settings array.
 *
 * @return string|bool
 *   The video iframe, or FALSE in case the ID can't be retrieved from the URL.
 */
function video_embed_urls_handle_video($url, $settings) {
  $id = _video_embed_urls_get_video_id($url);

  if ($id) {
    // Our embed code.
    //$embed='<iframe class="@class" src="@url" width="@width" height="@height"></iframe> ';
    $embed='
  <div class="video-player-field @class">
   <!-- player skin -->
   <link rel="stylesheet" href="/sites/all/libraries/flowplayer/skin/functional.css">

   <!-- site specific styling -->
   <style>
   .flowplayer { width: 80%; }
   </style>

   <!-- include flowplayer -->
   <script src="/sites/all/libraries/flowplayer/flowplayer.min.js"></script>

   <!-- the player -->
   <div class="flowplayer" data-swf="flowplayer.swf" data-ratio="0.4167">
      <video>
         <source type="video/mp4" src="@url">
      </video>
   </div>
  </div>
';
    // Use format_string to replace our placeholders with the settings values.
    $embed = format_string($embed, array(
      '!id' => $id,
      '@width' => $settings['width'],
      '@height' => $settings['height'],
      '@class' => $settings['class'],
      '@url' => $url,
    ));

    $video = array(
      '#markup' => $embed,
    );
    return $video;
  }

  return FALSE;
}

/**
 * Gets the thumbnail url for Allowed URLs videos.
 *
 * @param string $url
 *   The video URL.
 *
 * @return array
 *   The video thumbnail information.
 */
function video_embed_urls_handle_thumbnail($url) {
  $id = _video_embed_urls_get_video_id($url);

  // Temporary we will use httpbin to return default image.
  return array(
    'id' => $id,
    'url' => 'http://httpbin.org/image',
  );
}

/**
 * Helper function to get the Facebook video's id.
 *
 * @param string $url
 *   The video URL.
 *
 * @return string|bool
 *   The video ID, or FALSE in case the ID can't be retrieved from the URL.
 */
function _video_embed_urls_get_video_id($url) {
  // Returning random id for now.
  return time().microtime();
}

/**
 * Altering VEF settings form.
 */
function video_embed_urls_form_video_embed_field_settings_form_alter(&$form, &$form_state, $form_id){
  $form['video_embed_urls_subdomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed URLs'),
    '#default_value' => variable_get('video_embed_urls', ''),
  );
}

